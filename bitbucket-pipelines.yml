setup: &setup
  step:
    name: Setup testing resources
    script:
      - STACK_NAME="aws-code-deploy-ci-${BITBUCKET_BUILD_NUMBER}"
      - S3_BUCKET="${STACK_NAME}-codedeploy-deployment"
      - pipe: atlassian/aws-cloudformation-deploy:0.4.3
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          STACK_NAME: ${STACK_NAME}
          TEMPLATE: "./test/CloudFormationStackTemplate.yml"
          CAPABILITIES: ['CAPABILITY_IAM']
          WAIT: 'true'
          STACK_PARAMETERS: >
              [{
                "ParameterKey": "ApplicationName",
                "ParameterValue": "AWS-CODE-DEPLOY-CI-${BITBUCKET_BUILD_NUMBER}"
              },
              {
                "ParameterKey": "DeploymentGroupName",
                "ParameterValue": "${STACK_NAME}-group"
              },
              {
                "ParameterKey": "BucketName",
                "ParameterValue": "${S3_BUCKET}"
              },
              {
                "ParameterKey": "KeyName",
                "ParameterValue": "aws-pipes-tests"
              }]

test: &test
  parallel:
    - step:
        name: Test Deployment of Artifact
        oidc: true
        image: python:3.9
        services:
          - docker
        script:
          - pip install -r test/requirements.txt
          - flake8 --ignore E501,E125
          - pytest -p no:cacheprovider test/test_unit.py --verbose --capture=no
          - pytest test/test.py --verbose --capture=no
        after-script:
          - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.2.9.zip" -o "awscliv2.zip" && unzip awscliv2.zip
          - echo 'c778f4cc55877833679fdd4ae9c94c07d0ac3794d0193da3f18cb14713af615f awscliv2.zip' | sha256sum -c - && ./aws/install
          - STACK_NAME="aws-code-deploy-ci-${BITBUCKET_BUILD_NUMBER}"
          - S3_BUCKET="${STACK_NAME}-codedeploy-deployment"
          - aws --region ${AWS_DEFAULT_REGION} s3 rb s3://${S3_BUCKET} --force
          - aws --region ${AWS_DEFAULT_REGION} cloudformation delete-stack --stack-name ${STACK_NAME}

    - step:
        name: Lint the Dockerfile
        image: hadolint/hadolint:latest-debian
        script:
          - hadolint Dockerfile


release-dev: &release-dev
  step:
    name: Release development version
    image: python:3.9
    trigger: manual
    script:
      - pip install semversioner
      - VERSION=$(semversioner current-version).${BITBUCKET_BUILD_NUMBER}-dev
      - pipe: atlassian/bitbucket-pipe-release:4.0.1
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/$BITBUCKET_REPO_SLUG
          GIT_PUSH: 'false'
          VERSION: ${VERSION}
    services:
      - docker


push: &push
  step:
    name: Push and Tag
    script:
      - pipe: atlassian/bitbucket-pipe-release:4.0.1
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/${BITBUCKET_REPO_SLUG}
    services:
      - docker

pipelines:
  default:
  - <<: *setup
  - <<: *test
  - <<: *release-dev
  branches:
    master:
    - <<: *setup
    - <<: *test
    - <<: *push

  custom:
    test-and-report:
      - step:
          image: apk update && apk add bats zip curl
          services:
            - docker
          script:
            # Install bats
            - apk update && apk add bats zip
            # Build
            - export DOCKERHUB_IMAGE="bitbucketpipelines/${BITBUCKET_REPO_SLUG}"
            - export DOCKERHUB_TAG=${BITBUCKET_BUILD_NUMBER}
            - docker build -t ${DOCKERHUB_IMAGE}:${BITBUCKET_BUILD_NUMBER} -t ${DOCKERHUB_IMAGE}:latest .
            # Test
            - set +e
            - bats test/test*; test_exit_code=$?
            - set -e
            - echo test_exit_code = $test_exit_code
            - pipe: atlassian/report-task-test-result:1.0.0
              variables:
                DATADOG_API_KEY: $DATADOG_API_KEY
                TASK_NAME: aws-code-deploy
                TEST_EXIT_CODE: $test_exit_code
            - exit $test_exit_code
