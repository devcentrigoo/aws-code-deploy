import os
import time
import stat

import yaml
import boto3
from botocore.exceptions import ClientError, WaiterError
import configparser

from bitbucket_pipes_toolkit import Pipe, get_logger


WAIT_INTERVAL_DEFAULT = 15  # sec
WAIT_MAX_ATTEMPTS_DEFAULT = 120

schema_commands = ['upload', 'deploy']
allowed_file_extensions = ['zip', 'tar', 'tgz', 'yml', 'yaml', 'json', 'YAML', 'JSON']

schema = {
    # base
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': True},
    'COMMAND': {'type': 'string', 'required': True, 'allowed': schema_commands},
    'APPLICATION_NAME': {'type': 'string', 'required': True},
}

common_schema = {
    # common
    'BUNDLE_TYPE': {'type': 'string', 'required': False, 'allowed': allowed_file_extensions, 'default': 'zip'},
    'S3_BUCKET': {'type': 'string', 'required': False, 'default': "${APPLICATION_NAME_LOWER_CASE}-codedeploy-deployment"},
    'VERSION_LABEL': {'type': 'string', 'required': False, 'default': '${APPLICATION_NAME_LOWER_CASE}-${BITBUCKET_BUILD_NUMBER}-${BITBUCKET_COMMIT:0:8}'},
    'FOLDER': {'type': 'string', 'required': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
}

upload_schema = {
    # upload
    'ZIP_FILE': {'type': 'string', 'required': True},
}

deploy_schema = {
    # deploy
    'DEPLOYMENT_GROUP': {'type': 'string', 'required': True},
    'FILE_EXISTS_BEHAVIOR': {'type': 'string', 'required': False, 'default': 'DISALLOW'},
    'WAIT_INTERVAL': {'type': 'number', 'required': False, 'default': WAIT_INTERVAL_DEFAULT},
    'WAIT': {'type': 'boolean', 'required': False, 'default': True},
    'IGNORE_APPLICATION_STOP_FAILURES': {'type': 'boolean', 'required': False, 'default': False},
    'EXTRA_ARGS': {"type": "dict", "required": False},
}


logger = get_logger()


class CodeDeployPipe(Pipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'

    def __init__(self, *args, **kwargs):
        self.auth_method = self.discover_auth_method()
        self.schema = schema
        self.update_schema()
        super().__init__(*args, **kwargs)
        self.s3_bucket_key = f"{self.get_variable('FOLDER')}/{self.get_variable('VERSION_LABEL')}" if self.get_variable('FOLDER') else self.get_variable('VERSION_LABEL')
        self.revision = {
            'revisionType': 'S3',
            's3Location': {
                'bucket': self.get_variable('S3_BUCKET'),
                'key': self.s3_bucket_key,
                'bundleType': self.get_variable('BUNDLE_TYPE'),
            }
        }

    def update_schema(self):
        bitbucket_build_number = os.getenv('BITBUCKET_BUILD_NUMBER')
        bitbucket_commit = os.getenv('BITBUCKET_COMMIT')

        if os.getenv('APPLICATION_NAME'):
            application_name_lowercase = os.getenv('APPLICATION_NAME').lower()
        else:
            self.fail("Validation error: APPLICATION_NAME variable is required.")

        common_schema['S3_BUCKET']['default'] = f"{application_name_lowercase}-codedeploy-deployment"
        common_schema['VERSION_LABEL']['default'] = f"{application_name_lowercase}-{bitbucket_build_number}-{bitbucket_commit[0:8]}"
        self.schema.update(common_schema)

        command = os.getenv('COMMAND')
        if command not in schema_commands:
            self.fail(f'Validation error: Only {schema_commands} COMMAND supported.')
        elif command == 'upload':
            self.schema.update(upload_schema)
        elif command == 'deploy':
            self.schema.update(deploy_schema)

    def discover_auth_method(self):
        """Discover user intentions: authenticate to AWS through OIDC or default aws access keys"""
        oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if web_identity_token:
                logger.info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider')
                schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

                schema['AWS_ACCESS_KEY_ID']['required'] = False
                schema['AWS_SECRET_ACCESS_KEY']['required'] = False

                # unset env variables to prevent aws client general auth
                # https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html
                if 'AWS_ACCESS_KEY_ID' in os.environ:
                    del os.environ['AWS_ACCESS_KEY_ID']
                if 'AWS_SECRET_ACCESS_KEY' in os.environ:
                    del os.environ['AWS_SECRET_ACCESS_KEY']
                if 'AWS_SESSION_TOKEN' in os.environ:
                    del os.environ['AWS_SESSION_TOKEN']

                return self.OIDC_AUTH

            logger.warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')
            logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
            return self.DEFAULT_AUTH

        logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
        return self.DEFAULT_AUTH

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            random_number = str(time.time_ns())
            aws_config_directory = os.path.join(os.environ["HOME"], '.aws')
            oidc_token_directory = os.path.join(aws_config_directory, '.aws-oidc')

            os.makedirs(aws_config_directory, exist_ok=True)
            os.makedirs(oidc_token_directory, exist_ok=True)

            web_identity_token_path = os.path.join(oidc_token_directory, f'oidc_token_{random_number}')
            with open(web_identity_token_path, 'w') as f:
                f.write(self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'))

            os.chmod(web_identity_token_path, mode=stat.S_IRUSR)
            logger.debug('Web identity token file is created')

            aws_configfile_path = os.path.join(aws_config_directory, 'config')
            with open(aws_configfile_path, 'w') as configfile:
                config = configparser.ConfigParser()
                config['default'] = {
                    'role_arn': self.get_variable('AWS_OIDC_ROLE_ARN'),
                    'web_identity_token_file': web_identity_token_path
                }
                config.write(configfile)
            logger.debug('Configured settings for authentication with assume web identity role')

    def get_client(self, service='codedeploy'):
        try:
            return boto3.client(service, region_name=self.get_variable('AWS_DEFAULT_REGION'))
        except ClientError as error:
            self.fail(f"Failed to create boto3 client for {service}.\n" + str(error))

    def check_revision(self):
        try:
            self.get_client().get_application_revision(
                applicationName=self.get_variable('APPLICATION_NAME'),
                revision=self.revision,
            )
        except ClientError as error:
            self.fail("Failed to fetch revision..\n" + str(error))

    def get_deployment(self, deployment_id):
        try:
            return self.get_client().get_deployment(
                deploymentId=deployment_id,
            )
        except ClientError as error:
            self.fail("Failed to get deployment..\n" + str(error))

    def deploy(self):
        self.log_info("Deploying app from revision.")

        self.check_revision()

        deploy_parameters = {
            'applicationName': self.get_variable('APPLICATION_NAME'),
            'deploymentGroupName': self.get_variable('DEPLOYMENT_GROUP'),
            'description': f"Deployed from Bitbucket Pipelines using aws-code-deploy pipe. For details follow the link https://bitbucket.org/{self.env.get('BITBUCKET_REPO_OWNER')}/{self.env.get('BITBUCKET_REPO_SLUG')}/addon/pipelines/home#!/results/{self.env.get('BITBUCKET_BUILD_NUMBER')}",
            'revision': self.revision,
            'ignoreApplicationStopFailures': self.get_variable('IGNORE_APPLICATION_STOP_FAILURES')
        }

        if self.get_variable('BUNDLE_TYPE') in ['zip', 'tar', 'tgz']:
            deploy_parameters.update({'fileExistsBehavior': self.get_variable('FILE_EXISTS_BEHAVIOR')})

        if 'EXTRA_ARGS' in self.variables:
            deploy_parameters.update(self.get_variable('EXTRA_ARGS'))

        deploy_client = self.get_client()
        try:
            response = deploy_client.create_deployment(**deploy_parameters)
        except ClientError as error:
            self.fail("Failed to create deployment.\n" + str(error))

        self.log_info(f"Deployment started. Use this link to access the deployment in the AWS console: https://console.aws.amazon.com/codesuite/codedeploy/deployments/{response['deploymentId']}?region={self.get_variable('AWS_DEFAULT_REGION')}")

        if self.get_variable('WAIT'):
            self.log_info('"Waiting for deployment to complete."')

            waiter = deploy_client.get_waiter('deployment_successful')
            wait_interval = self.get_variable('WAIT_INTERVAL')
            try:
                waiter.wait(
                    deploymentId=response['deploymentId'],
                    WaiterConfig={
                        'Delay': wait_interval,
                        'MaxAttempts': WAIT_MAX_ATTEMPTS_DEFAULT
                    }
                )
            except WaiterError as error:
                self.log_error("Deployment failed. Fetching deployment information...")
                self.log_info(self.get_deployment(response['deploymentId']))
                self.fail(f'Error waiting for deployment successful: {error}')

            self.success("Deployment completed successfully.")
        else:
            self.success("Skip waiting for deployment to complete.")

    def upload_to_s3(self):
        self.log_info(f"Uploading {self.get_variable('ZIP_FILE')} to S3.")
        s3_client = self.get_client('s3')
        try:
            with open(self.get_variable('ZIP_FILE'), 'rb') as zip_file:
                s3_client.put_object(
                    Body=zip_file,
                    Bucket=self.get_variable('S3_BUCKET'),
                    Key=self.s3_bucket_key
                )
        except ClientError as error:
            self.fail(f"Failed to upload {self.get_variable('ZIP_FILE')} to S3 \n" + str(error))

        self.log_info("Registering a revision for the artifact.")
        try:
            self.get_client().register_application_revision(
                applicationName=self.get_variable('APPLICATION_NAME'),
                revision=self.revision,
            )
        except ClientError as error:
            self.fail("Failed to register application revision.\n" + str(error))

        self.success("Application uploaded and revision created.")

    def run(self):
        super().run()
        self.log_info('Executing the aws-ecr-push-image pipe...')
        if self.get_variable('COMMAND') == 'upload':
            self.upload_to_s3()
        else:
            self.deploy()


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = CodeDeployPipe(pipe_metadata=metadata, schema=schema, logger=logger, check_for_newer_version=True)
    pipe.auth()
    pipe.run()
