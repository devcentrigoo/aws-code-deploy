# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.1

- patch: Internal maintenance: Add required tags for test infra resources.

## 1.1.0

- minor: Add support for WAIT_INTERVAL variable for deploy state. Now you could tune time to wait between polling for deployment to complete (in seconds). Default: '15'.

## 1.0.0

- major: Migrate pipe's codebase to Python.

## 0.6.0

- minor: Bump aws-cli -> 1.18.188.
- minor: Support AWS OIDC authentication. Environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY are not required anymore.

## 0.5.4

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.5.3

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.5.2

- patch: Internal maintenance: Add gitignore secrets.

## 0.5.1

- patch: Internal maintenance: Update auto infrastructure for tests.

## 0.5.0

- minor: Add FOLDER parameter to support the upload and deployment of application from a folder inside S3 bucket

## 0.4.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 0.4.0

- minor: Add default values for AWS variables.

## 0.3.2

- patch: Add link to the pipeline in description.

## 0.3.1

- patch: Fix bug in order to prevent creating S3 buckets with uppercase characters.

## 0.3.0

- minor: Add BUNDLE_TYPE parameter to support different application's bundle in S3: zip, tar, tgz, YAML, JSON.

## 0.2.11

- patch: Print link to deployment in logs

## 0.2.10

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.2.9

- patch: Internal maintenance: Refactor tests unique stacks.
- patch: Internal release

## 0.2.8

- patch: Improved the release process.
- patch: Internal CI bugfix
- patch: Internal CI bugfix

## 0.2.7

- patch: Documentation updates

## 0.2.6

- patch: Update aws-cli base docker image version.
- patch: Update pipes bash toolkit version.

## 0.2.5

- patch: Fixed the issue with deploying large projects

## 0.2.4

- patch: Minor documentations updates

## 0.2.3

- patch: Fixed the bug that caused successfull builds to crash

## 0.2.2

- patch: Updated contributing guidelines

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Add support for DEBUG parameter.
- minor: Rename from tasks to pipes.

## 0.1.2

- patch: Use quotes for all pipes examples in README.md.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines AWS CodeDeploy deployment pipe.

