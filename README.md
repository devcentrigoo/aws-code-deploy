# Bitbucket Pipelines Pipe: AWS CodeDeploy 

Perform an EC2 deployment using AWS CodeDeploy.


## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:    

```yaml
- pipe: atlassian/aws-code-deploy:1.1.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    AWS_OIDC_ROLE_ARN: '<string>' # Optional by default. Required for OpenID Connect (OIDC) authentication.
    APPLICATION_NAME: '<string>'
    COMMAND: '<string>' # 'upload' or 'deploy'.

    # Common variables
    # S3_BUCKET: '<string>' # Optional.
    # VERSION_LABEL: '<string>' # Optional.
    # BUNDLE_TYPE: '<string>' # Optional.
    # FOLDER: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.

    # Upload variables
    # ZIP_FILE: '<string>'

    # Deploy variables
    # DEPLOYMENT_GROUP: '<string>'
    # FILE_EXISTS_BEHAVIOR: '<string>' # Optional.
    # IGNORE_APPLICATION_STOP_FAILURES: '<boolean>' # Optional.
    # WAIT: '<boolean>' # Optional.
    # WAIT_INTERVAL: '<integer>' # Optional.
    # EXTRA_ARGS: '<json>' # Optional.
```


## Variables

### Common Variables

| Variable                    | Usage |
| --------------------------- | ----- |
| AWS_ACCESS_KEY_ID (**)       | AWS access key id. |
| AWS_SECRET_ACCESS_KEY (**)   | AWS secret key. |
| AWS_DEFAULT_REGION (**)      | The AWS region code (`us-east-1`, `us-west-2`, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints](https://docs.aws.amazon.com/general/latest/gr/rande.html) in the _Amazon Web Services General Reference_. |
| AWS_OIDC_ROLE_ARN            | The ARN of the role used for web identity federation or OIDC. See **Authentication**. |
| APPLICATION_NAME (*)        | Application name. |
| COMMAND (*)                 | Mode of operation: `upload` or `deploy`. See the **Details** section to understand how each mode works. |
| BUNDLE_TYPE                 | The [file type](https://docs.aws.amazon.com/codedeploy/latest/APIReference/API_S3Location.html) of the application revision stored in S3: `zip`, `tar`, `tgz`, `YAML` or `JSON`.  Default: `zip`. BUNDLE_TYPE should correspond to ZIP_FILE extension. |
| DEBUG                       | Turn on extra debug information. Default: `false`. |
| FOLDER                      | If the deployable artifact is in any folder inside bucket, specify the folder name|

_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


### Upload Command Variables
If `COMMAND` is set to `upload`: 

| Variable                    | Usage |
| --------------------------- | ----- |
| ZIP_FILE (*)                | The application artifact to upload to S3. Required for 'update'. Supported [file types](https://docs.aws.amazon.com/codedeploy/latest/APIReference/API_S3Location.html): `zip`, `tar`, `tgz`, `YAML` or `JSON`. File extension should correspond to BUNDLE_TYPE. |
| S3_BUCKET                   | Override the S3 bucket that the application zip is uploaded to and deployed from. The default follows the convention `<application_name>-codedeploy-deployment` |
| VERSION_LABEL               | Override the name of the application revision in S3. The default follows the convention `<application_name>-<build_number>-<commit>` |
_(*) = required variable. This variable needs to be specified always when using the pipe._


### Deploy Command Variables
If `COMMAND` is set to `deploy`: 

| Variable                    | Usage |
| --------------------------- | ----- |
| DEPLOYMENT_GROUP (*)        | Name of the Deployment Group. |
| S3_BUCKET                   | Override the S3 bucket that the application zip is uploaded to and deployed from. The default follows the convention `<application_name>-codedeploy-deployment` |
| VERSION_LABEL               | Override the name of the application revision in S3. The default follows the convention `<application_name>-<build_number>-<commit>` |
| WAIT                        | Wait for the deployment to complete. Default: `true`. |
| WAIT_INTERVAL               | Time to wait between polling for deployment to complete (in seconds). Default: `15`. |
| FILE_EXISTS_BEHAVIOR        | Action to take if files already exist in the deployment target location (defined in the AppSpec file). Allowed values: `OVERWRITE`, `DISALLOW`, `RETAIN`, default: `DISALLOW`. |
| IGNORE_APPLICATION_STOP_FAILURES | Ignore any errors thrown when trying to stop the previous version of the deployed application. Default: `false`. |
| EXTRA_ARGS                  | JSON document containing other supported [aws codedeploy create deployment][aws codedeploy create deployment] parameters. The value should be a dictionary(mapping) {"key": "value"}. The **Examples** section below contains an example for passing parameters to the stack. | |
_(*) = required variable. This variable needs to be specified always when using the pipe._



## Details

The pipe provides 2 modes of operation:

**Upload**

Upload the application (as a zip file) to an S3 bucket, and register a new application revision with CodeDeploy.

By default, the zip file is uploaded to an S3 bucket following the naming convention ```<application_name>-codedeploy-deployment```, which can be overridden
with the `S3_BUCKET` parameter.

The uploaded zip artifact will be named `<application_name>-<build_number>-<commit>`, which can be overridden with the `VERSION_LABEL` parameter.
 

**Deploy**

Deploy a previously uploaded application revision to a deployment group.

By default, the revision S3 bucket containing the revision follows the naming convention ```<application_name>-codedeploy-deployment```, which can be overridden
with the `S3_BUCKET` parameter.

The pipe will attempt to deploy the application revision matching `<application_name>-<build_number>-<commit>`, which can be overridden with the `VERSION_LABEL` parameter,
and wait until deployment has succeeded.

**Caveats**

*  When you use the `deploy` mode with the default `VERSION_LABEL`, the pipe will generate a new version label based on the build number and commit hash, so you need to make sure to also run the pipe
with the `upload` mode withing the same pipeline so the corresponding version is preset in S3. If you don't run the `upload` part of the pipe in the same pipeline, you should use explicit `VERSION_LABEL`,
for example, use semantic or other versioning scheme that is decoupled from the build number.
 

## Authentication

Supported options:

1. Environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Default option.

2. Assume role provider with OpenID Connect (OIDC). More details in the Bitbucket Pipelines Using OpenID Connect guide [Integrating aws bitbucket pipeline with oidc][aws-oidc]. Make sure that you setup OIDC before:
    * configure Bitbucket Pipelines as a Web Identity Provider in AWS
    * attach to provider your AWS role with required policies in AWS
    * setup a build step with `oidc: true` in your Bitbucket Pipelines
    * pass AWS_OIDC_ROLE_ARN (*) variable that represents role having appropriate permissions to execute actions on Code Deploy resources


## Prerequisites
* An IAM user configured with programmatic access or Web Identity Provider OIDC role is configured with sufficient permissions to allow the pipe to perform a deployment to your application and upload artifacts to the S3 bucket.
* You have configured a CodeDeploy Application and Deployment Group. Here is a simple tutorial from AWS: [Deploy Code to a Virtual Machine](https://aws.amazon.com/getting-started/tutorials/deploy-code-vm/)
* An S3 bucket has been set up to which deployment artifacts will be copied.


## Examples

### Upload
Upload the application `myapp.zip` to S3 bucket called `my-application-codedeploy-deployment`, with the application uploaded to S3 as `my-application-<build-number>-<commit>`.
 
```yaml
script:
  - pipe: atlassian/aws-code-deploy:1.1.1
    variables:
      AWS_DEFAULT_REGION: 'ap-southeast-2'
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      COMMAND: 'upload'
      APPLICATION_NAME: 'my-application'
      ZIP_FILE: 'myapp.zip'
```

Upload the application `myapp.zip` to S3 bucket with OpenID Connect (OIDC) alternative authentication without required `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`.
Parameter `oidc: true` in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required:

```yaml
- step:
    oidc: true
    script:
      - pipe: atlassian/aws-code-deploy:1.1.1
        variables:
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
          COMMAND: 'upload'
          APPLICATION_NAME: 'my-application'
          ZIP_FILE: 'myapp.zip'
```

Upload the application `myapp.zip` to custom S3 bucket called `my-bucket`, with the application uploaded to S3 as `my-app-1.0.0`.


```yaml
script:
  - pipe: atlassian/aws-code-deploy:1.1.1
    variables:
      AWS_DEFAULT_REGION: 'ap-southeast-2'
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      COMMAND: 'upload'
      APPLICATION_NAME: 'my-application'
      ZIP_FILE: 'myapp.zip'
      S3_BUCKET: 'my-bucket'
      VERSION_LABEL: 'my-app-1.0.0'
```

Upload the application `myapp.zip` to custom S3 bucket called `my-bucket`, with the application uploaded to S3 as `my-app-1.0.0`.
`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.


```yaml
script:
  - pipe: atlassian/aws-code-deploy:1.1.1
    variables:
      COMMAND: 'upload'
      APPLICATION_NAME: 'my-application'
      ZIP_FILE: 'myapp.zip'
      S3_BUCKET: 'my-bucket'
      VERSION_LABEL: 'my-app-1.0.0'
```


### Deploy
Start a deployment and wait for it to finish. The application revision `my-application-<build-number>-<commit>` from the S3 bucket `my-application-codedeploy-deployment` will be deployed.

```yaml
script:
  - pipe: atlassian/aws-code-deploy:1.1.1
    variables:
      AWS_DEFAULT_REGION: 'ap-southeast-2'
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      COMMAND: 'deploy'
      APPLICATION_NAME: 'my-application'
      DEPLOYMENT_GROUP: 'my-deployment-group'
      WAIT: 'true'
```

Start a deployment with OpenID Connect (OIDC) alternative authentication without required `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`.
Parameter `oidc: true` in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required:

```yaml
- step:
    oidc: true
    script:
      - pipe: atlassian/aws-code-deploy:1.1.1
        variables:
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
          COMMAND: 'upload'
          APPLICATION_NAME: 'my-application'
          ZIP_FILE: 'myapp.zip'
```

Start a deployment with EXTRA_ARGS and wait for it to finish.
The application revision `my-application-<build-number>-<commit>` from the S3 bucket `my-application-codedeploy-deployment` will be deployed.

```yaml
script:
  - pipe: atlassian/aws-code-deploy:1.1.1
    variables:
      AWS_DEFAULT_REGION: 'ap-southeast-2'
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      COMMAND: 'deploy'
      APPLICATION_NAME: 'my-application'
      DEPLOYMENT_GROUP: 'my-deployment-group'
      WAIT: 'true'
      EXTRA_ARGS: >
         {
            "description": "My deployment"
         }
        
```

Start a deployment, referencing a application revision uploaded to a custom location in S3. The application revision `my-app-1.0.0` from the S3 bucket `my-bucket` will be deployed.

```yaml
script:
  - pipe: atlassian/aws-code-deploy:1.1.1
    variables:
      AWS_DEFAULT_REGION: 'ap-southeast-2'
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      COMMAND: 'deploy'
      APPLICATION_NAME: 'my-application'
      DEPLOYMENT_GROUP: 'my-deployment-group'
      WAIT: 'true'
      S3_BUCKET: 'my-bucket'
      VERSION_LABEL: 'my-app-1.0.0'
```

Start a deployment, and ignore any application stop failures, and force overwrite previous deployment files.
```yaml
script:
  - pipe: atlassian/aws-code-deploy:1.1.1
    variables:
      AWS_DEFAULT_REGION: 'ap-southeast-2'
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      COMMAND: 'deploy'
      APPLICATION_NAME: 'my-application'
      DEPLOYMENT_GROUP: 'my-deployment-group'
      WAIT: 'true'
      IGNORE_APPLICATION_STOP_FAILURES: 'true'
      FILE_EXISTS_BEHAVIOR: 'OVERWRITE'
```


Start a deployment, referencing a application revision uploaded to a custom location in S3. The application revision `lambda-app-1.0.0` from the S3 bucket `my-bucket` will be deployed.

```yaml
script:
  - pipe: atlassian/aws-code-deploy:1.1.1
    variables:
      AWS_DEFAULT_REGION: 'ap-southeast-2'
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      COMMAND: 'deploy'
      APPLICATION_NAME: 'lambda-application'
      DEPLOYMENT_GROUP: 'my-deployment-group'
      WAIT: 'true'
      S3_BUCKET: 'my-bucket'
      VERSION_LABEL: 'lambda-app-1.0.0'
      BUNDLE_TYPE: 'YAML'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,aws,code-deploy
[aws-oidc]: https://support.atlassian.com/bitbucket-cloud/docs/deploy-on-aws-using-bitbucket-pipelines-openid-connect
[aws codedeploy create deployment]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/codedeploy.html#CodeDeploy.Client.create_deployment

